from random import uniform
from entity import Entity

class Attack():

    def __init__(self, power, atype):
        """
        The attack class determine all the attacks components

        Parameters
        ----------
        power : int
            amount of power of the attack
        atype : string
            the type of the attack
        """
        self.power = power
        self.atype = atype

    def compute_dealt_damage(self,target:Entity):
        """
        Compute damage dealt (or not) to a target

        :param target: An entity targeted by the attack
        :type target: Entity
        :return: Hit and damage dealt
        :rtype: Tuple (Boolean, Int)

        Parameters
        ----------
        target : Entity
            the entity on which the attack is made

        Returns
        -------
        tuple
            (true, <damages>) where damages is the amount of damage in cas of a successfull attack
        """
        # test for the dodge
        hit = uniform(0,1) > target.statistics['dodge']
        if not hit:
            return (False,0)
        # compute damage dealt
        if self.atype == "magical":
            return (True,-int(self.power - self.power*target.statistics['resistance']))
        return (True,-self.power)
