#!/usr/bin/env python3
from adventurer import Adventurer
from random import randint
from itertools import product

import csv

def brawl(A: Adventurer,B: Adventurer):

    max_turn = 10
    turn_count = 1
    if randint(0,1):
        (p1,p2) = (A,B)
    else:
        (p1,p2) = (B,A)
    
    #FIGHT
    while True:
        #p1 attack p2, check if p2 died
        atk = p1.do_attack(p2)
        if atk[0]:
            p2.update_hp(atk[1])
            if p2.statistics['local_hp'] == 0:
                return(p1.name)
        #p2 attack p1, check if p1 died
        atk = p2.do_attack(p1)
        if atk[0]:
            p1.update_hp(atk[1])
            if p1.statistics['local_hp'] == 0:
                return(p2.name)
        #check if turn we hit the turn limit
        if turn_count == max_turn:
            return("X")
        turn_count += 1

def print_fight_stat(stat_fight,fname="out.csv"):
    with open(fname,"w") as f:
        writer = csv.writer(f)
        #header
        header = ("strA","agiA","intA",
                  "strB","agiB","intB",
                  "winA","winB","draw")
        writer.writerow(header)
        #data
        for key in stat_fight:
            writer.writerow(key + (stat_fight[key]["A"],
                         stat_fight[key]["B"],
                         stat_fight[key]["X"]))

def test_brawl(stat_begin=1,stat_end=10,stat_step=1,nrep=10):
    #generate all stats:
    stat_number = 3
    stat_range = range(stat_begin,stat_end + 1,stat_step)
    stat_iter = list(product(stat_range,repeat=stat_number))
    #store fight
    total_fight = {}
    for sA in stat_iter:
        print(sA)
        for sB in stat_iter:
            #generate adventurer
            A = Adventurer('A',sA[0],sA[1],sA[2])
            B = Adventurer('B',sB[0],sB[1],sB[2])
            count_win = {"A":0,"B":0,"X":0}
            #brawl
            for rep in range(nrep):
                count_win[brawl(A,B)] +=1
            total_fight[sA+sB] = count_win
    return(total_fight)
    

        
    
    
def main():
    #stat
    out = test_brawl(25,125,25,100)
    print_fight_stat(out,"out.csv")

if __name__ == "__main__":
    main()
