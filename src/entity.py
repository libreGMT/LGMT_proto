class Entity():

    base_statistics = {
        'hp': 20,
        'physical_power': 5,
        'magical_power': 5,
        'dodge': 0.2,
        'resistance': 0.1
    }

    def __init__(self, strength, agility, intelligence, level=1):
        """
        An entity is having different attributes and can start at selected level

        Parameters
        ----------
        strength : int
            the strength determine both physical damage and health points
        agility : int
            the agility gives the dodging chances and success of hitting
        intelligence : int
            the intelligence gives magic damage, magic resistance and status resistance
        level : int, optional
            level at which starts the entity, by default 1
        """
        self.statistics = {}
        self.strength = strength
        self.agility = agility
        self.intelligence = intelligence
        self.level = level
        self.update_statistics()

    def update_statistics(self):
        """
        Update the entity statistics based on its abilities levels
        """
        # Health point
        self.statistics['hp'] = int(self.base_statistics['hp'] + self.strength**2)
        # Physical power
        self.statistics['physical_power'] = int(self.base_statistics['physical_power'] + \
            (self.base_statistics['physical_power'] * self.strength) / 2)
        # Magical power
        self.statistics['magical_power'] = int(self.base_statistics['magical_power'] + \
            (self.base_statistics['magical_power'] * self.intelligence) / 2)
        # Dodge ratio
        self.statistics['dodge'] = self.base_statistics['dodge'] + \
            self.agility/100
        if (self.statistics['dodge'] > 0.99):
            self.statistics['dodge'] = 0.99
        # Magical resistance ratio
        self.statistics['resistance'] = self.base_statistics['resistance'] + \
            self.intelligence/100
        if (self.statistics['resistance'] > 0.99):
            self.statistics['resistance'] = 0.99

