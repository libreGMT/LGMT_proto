from entity import Entity
import attack


class Adventurer(Entity):
    def __init__(self, name, strength, agility, intelligence):
        """
        An adventurer is an entity which is meant to evolve through time and fights.
        He has different abilities that can be boosted.

        Parameters
        ----------
        Entity : class
        name : string
            Name of the adventurer
        strength : int
            the strength determine both physical damage and health points
        agility : int
            the agility gives the dodging chances and success of hitting
        intelligence : int
            the intelligence gives magic damage, magic resistance and status resistance
        """
        Entity.__init__(self, strength, agility, intelligence)
        self.name = name
        self.set_hp()

    def set_hp(self):
        """
        Sets the starting health points of the adventurer
        """
        self.statistics['local_hp'] = self.statistics['hp']

    def update_hp(self, amount):
        """
        Update the actual amount of health points of the adventurer

        Parameters
        ----------
        amount : int
            positive, heal, or negative, damages, to add to the actual amount of health points
        """
        self.statistics['local_hp'] += amount
        if self.statistics['local_hp'] < 0:
            self.statistics['local_hp'] = 0
        elif self.statistics['local_hp'] > self.statistics['hp']:
            self.statistics['local_hp'] = self.statistics['hp']

    def do_attack(self, target: Entity):
        """
        Attack the targeted entity

        Parameters
        ----------
        target : Entity
            the entity to attack

        Returns
        -------
        int
            amount of damage dealts to the target
        """
        # test which attack is higher
        if self.statistics['physical_power'] > self.statistics['magical_power']:
            atk = attack.Attack(self.statistics['physical_power'], 'physical')
        else:
            atk = attack.Attack(self.statistics['magical_power'], 'magical')
        return atk.compute_dealt_damage(target)

    def __str__(self):
        """
        Printing output of the adventurer class

        Returns
        -------
        string
        """
        return self.name + ", " + str(self.statistics['local_hp']) + " local hp"